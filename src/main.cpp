#include <Arduino.h>
#include <Adafruit_NeoPixel.h>

// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1:
#define LED_PIN    6

// How many NeoPixels are attached to the Arduino?
#define LED_COUNT 512

// Declare our NeoPixel strip object:
Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);
// Argument 1 = Number of pixels in NeoPixel strip
// Argument 2 = Arduino pin number (most are valid)
// Argument 3 = Pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
//   NEO_RGBW    Pixels are wired for RGBW bitstream (NeoPixel RGBW products)

#define X_MAX 32
#define Y_MAX 16

unsigned long worldA[Y_MAX];
unsigned long worldB[Y_MAX];

void updateWorld(unsigned long oldWorld[Y_MAX], unsigned long newWorld[Y_MAX]) {
  int neighbors;
  bool currentlyAlive;
  for(int x = 0; x < X_MAX; x++) {
    for(int y = 0; y < Y_MAX; y++) {
      currentlyAlive = bitRead(oldWorld[y], x);
      neighbors = 0;
      
      int leftIndex = x - 1;
      if (leftIndex < 0) { leftIndex = X_MAX; }
      int rightIndex = x + 1;
      if (rightIndex > X_MAX) { rightIndex = 0; }
      int upperIndex = y + 1;
      if (upperIndex > Y_MAX) { upperIndex = 0; }
      int lowerIndex = y - 1;
      if (lowerIndex < 0) { lowerIndex = Y_MAX; }

      neighbors += bitRead(oldWorld[upperIndex], leftIndex);
      neighbors += bitRead(oldWorld[upperIndex], x);
      neighbors += bitRead(oldWorld[upperIndex], rightIndex);

      neighbors += bitRead(oldWorld[y], rightIndex);
      neighbors += bitRead(oldWorld[y], leftIndex);

      neighbors += bitRead(oldWorld[lowerIndex], leftIndex);
      neighbors += bitRead(oldWorld[lowerIndex], x);
      neighbors += bitRead(oldWorld[lowerIndex], rightIndex);

      if(currentlyAlive) {
        if(neighbors < 2 || neighbors > 3) {
          bitWrite(newWorld[y], x, 0);
        }
        else {
          bitWrite(newWorld[y], x, 1);
        }
      }
      else {
        //currently dead
        if(neighbors == 3) {
          bitWrite(newWorld[y], x, 1);
        }
        else {
          bitWrite(newWorld[y], x, 0);
        }
      }
    }
  }
}

void setPixel(int x, int y, int red, int green, int blue) {
  int pixel;
  int xPart;
  if ((y % 2) == 1) {
    //reverse odd rows from 32
    xPart = 31 - x;
  }
  else {
    //regular order for even numbered rows
    xPart = x;
  }
  pixel = (y * 32) + xPart;
  strip.setPixelColor(pixel, red, green, blue);
}


void setup() {
  // put your setup code here, to run once:
  pinMode(LED_BUILTIN, OUTPUT);
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'

  randomSeed(analogRead(5));

  for(int x = 0; x < X_MAX; x++) {
    for(int y = 0; y < Y_MAX; y++) {
      bitWrite(worldA[y], x, random(0, 2));
    }
  }
}

void dumpWorld(unsigned long world[Y_MAX]) {
  for(int i = 0; i < X_MAX; i++) {
    for(int j = 0; j < Y_MAX; j++) {
      if(bitRead(world[j], i)) {
        setPixel(i, j, 0, 0, 128);
      }
      else {
        setPixel(i, j, 0, 0, 0);
      }
    }
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  dumpWorld(worldA);
  strip.show();
  updateWorld(worldA, worldB);
  dumpWorld(worldB);
  strip.show();
  updateWorld(worldB, worldA);
}